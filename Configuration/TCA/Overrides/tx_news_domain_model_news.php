<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$columns = array(
	'recurring_parent' => array(
		'label' => 'recurring_parent',
		'config' => array(
			'type' => 'passthrough'
		)
	),
	'recurring_original' => array(
		'label' => 'recurring_original',
		'config' => array(
			'type' => 'passthrough'
		)
	),
	'recurring' => array(
		'exclude' => TRUE,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'LLL:EXT:news_recurring/Resources/Private/Language/locallang_db.xlf:tx_news_domain_model_news.recurring',
		'config' => array(
			'type' => 'inline',
			'allowed' => 'tx_news_domain_model_news',
			'foreign_table' => 'tx_news_domain_model_news',
			'foreign_sortby' => 'sorting',
			'foreign_field' => 'recurring_parent',
			'foreign_record_defaults' => array(
				'type' => 7
			),
			'size' => 5,
			'minitems' => 0,
			'maxitems' => 100,
			'appearance' => array(
				'collapseAll' => 1,
				'expandSingle' => 1,
				'levelLinksPosition' => 'bottom',
				'useSortable' => 1,
				'showPossibleLocalizationRecords' => 1,
				'showRemovedLocalizationRecords' => 1,
				'showAllLocalizationLink' => 1,
				'showSynchronizationLink' => 1,
				'enabledControls' => array(
					'info' => FALSE,
				)
			)
		)
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $columns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'recurring', '', 'after:datetime');

$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['type']['config']['items'][] = array('Recurring', 7);

$GLOBALS['TCA']['tx_news_domain_model_news']['types'][7] = array(
	'showitem' => 'l10n_parent, l10n_diffsource,type,datetime'
);

$GLOBALS['TCA']['tx_news_domain_model_news']['ctrl']['formattedLabel_userFunc'] =
	'GeorgRinger\\NewsRecurring\\Hooks\\Label->getNewsLabel';
<?php
namespace GeorgRinger\NewsRecurring\Tests\Unit\Persistence\Generic\Mapper;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class DataMapperTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function overlayIsDoneCorrectly() {
		$hookObject = $this->getAccessibleMock(\GeorgRinger\NewsRecurring\Persistence\Generic\Mapper\DataMapper::class, array('dummy'));

		$parent = [
			'uid' => 1,
			'title' => 'parent row',
			'datetime' => 1417374488,
			'type' => 0,
			'recurring_parent' => 0,
			'recurring' => 3
		];
		$current = [
			'uid' => 2,
			'title' => '',
			'datetime' => 1417374489,
			'type' => 7,
			'recurring_parent' => 1,
			'recurring' => 0
		];
		$expected = [
			'uid' => 1,
			'title' => 'parent row',
			'datetime' => 1417374489,
			'type' => 7,
			'recurring_parent' => 1,
			'recurring' => 3,
			'recurring_original' => 2
		];
		$out = $hookObject->_call('overlayRow', $parent, $current);
		$this->assertEquals($expected, $out);
	}

	/**
	 * @test
	 */
	public function customDataMapperIsCalledWithRecurringDomainModel() {
		$mockedIdentityMap = $this->getMock(\TYPO3\CMS\Extbase\Persistence\Generic\IdentityMap::class, array('registerObject'));
		$object = $this->getAccessibleMock(\GeorgRinger\NewsRecurring\Persistence\Generic\Mapper\DataMapper::class, array('overlayRow', 'getParentRow', 'createEmptyObject', 'thawProperties'));
		$object->_set('identityMap', $mockedIdentityMap);
		$row = [
			'uid' => 456,
			'title' => 'recurring news',
			'recurring_parent' => 123
		];
		$parentRow = [
			'uid' => 123,
			'title' => 'recurring news',
			'recurring_parent' => 123
		];
		$newsModel = new \Tx_NewsRecurring_Domain_Model_News();

		$object->expects($this->once())
			->method('overlayRow')->with()->willReturn($parentRow);
		$object->expects($this->once())
			->method('thawProperties')->with($newsModel, $parentRow);
		$object->expects($this->once())
			->method('getParentRow')->with('123')->willReturn($parentRow);
		$object->expects($this->once())
			->method('createEmptyObject')->willReturn($newsModel);

		$object->_call('mapSingleRow', 'Tx_NewsRecurring_Domain_Model_News', $row);
	}

}
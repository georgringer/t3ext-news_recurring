<?php
namespace GeorgRinger\NewsRecurring\Tests\Unit\Hooks;
use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class LabelTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function defaultTypeUsesRowTitleAsTitle() {
		/** @var \GeorgRinger\NewsRecurring\Hooks\Label $hookObject */
		$hookObject = $this->getMock(\GeorgRinger\NewsRecurring\Hooks\Label::class, array('dummy'));

		$params = array(
			'row' => array(
				'type' => 0,
				'title' => 'fobar'
			)
		);
		$hookObject->getNewsLabel($params);
		$this->assertEquals('fobar', $params['title']);
	}

	/**
	 * @test
	 */
	public function recurringTypeWithDateTimeUsesDateAsTitle() {
		/** @var \GeorgRinger\NewsRecurring\Hooks\Label $hookObject */
		$hookObject = $this->getMock(\GeorgRinger\NewsRecurring\Hooks\Label::class, array('dummy'));

		$params = array(
			'row' => array(
				'type' => 7,
				'title' => 'lorem ipsum',
				'datetime' => 1417200612
			)
		);
		$hookObject->getNewsLabel($params);
		$this->assertEquals('28-11-14 19:50', $params['title']);
	}

	/**
	 * @test
	 */
	public function recurringTypeWithNoDateTimeUsesCurrentDateAsTitle() {
		/** @var \GeorgRinger\NewsRecurring\Hooks\Label $hookObject */
		$hookObject = $this->getMock(\GeorgRinger\NewsRecurring\Hooks\Label::class, array('dummy'));

		$params = array(
			'row' => array(
				'type' => 7,
				'title' => 'lorem ipsum',
				'datetime' => 0
			)
		);
		$hookObject->getNewsLabel($params);
		$this->assertEquals(BackendUtility::datetime($GLOBALS['EXEC_TIME']), $params['title']);
	}
}
<?php
namespace GeorgRinger\NewsRecurring\Tests\Unit\Hooks;
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class DatabaseRecordListTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function whereClauseGetsExtended() {
		/** @var \GeorgRinger\NewsRecurring\Hooks\DatabaseRecordList $hookObject */
		$hookObject = $this->getMock(\GeorgRinger\NewsRecurring\Hooks\DatabaseRecordList::class, array('dummy'));
		$recordListMock = $this->getMock(\TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList::class, array('dummy'));

		$table = 'tx_news_domain_model_news';
		$pageId = 123;
		$additionalWhereClause = 'fo=bar';
		$selectedFieldsList = '';
		$hookObject->getDBlistQuery($table, $pageId, $additionalWhereClause, $selectedFieldsList, $recordListMock);

		$this->assertEquals('fo=bar AND type != 7', $additionalWhereClause);
	}
}
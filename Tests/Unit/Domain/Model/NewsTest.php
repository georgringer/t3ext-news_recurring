<?php
namespace GeorgRinger\NewsRecurring\Tests\Unit\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class NewsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Tx_NewsRecurring_Domain_Model_News
	 */
	protected $object;

	public function setUp() {
		$this->object = new \Tx_NewsRecurring_Domain_Model_News();
	}

	/**
	 * @test
	 */
	public function recurringCanBeSet() {
		$value = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();

		$item1 = new \Tx_News_Domain_Model_News();
		$item1->setTitle('Item 1');
		$value->attach($item1);
		$item2 = new \Tx_News_Domain_Model_News();
		$item2->setTitle('Item 2');
		$value->attach($item2);

		$this->object->setRecurring($value);

		$this->assertEquals($value, $this->object->getRecurring());
	}


	/**
	 * @test
	 */
	public function recurringParentCanBeSet() {
		$value = new \Tx_News_Domain_Model_News();
		$value->setTitle('Lorem ipsum');

		$this->object->setRecurringParent($value);

		$this->assertEquals($value, $this->object->getRecurringParent());
	}

	/**
	 * @test
	 */
	public function recurringOriginalCanBeSet() {
		$value = 123;

		$this->object->setRecurringOriginal($value);

		$this->assertEquals($value, $this->object->getRecurringOriginal());
	}

}
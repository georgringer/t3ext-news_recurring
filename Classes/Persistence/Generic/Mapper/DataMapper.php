<?php

namespace GeorgRinger\NewsRecurring\Persistence\Generic\Mapper;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class DataMapper extends \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper {

	/**
	 * Fields which needs to be from the original record
	 *
	 * @var array
	 */
	protected $overlaidFields = array('type', 'datetime', 'recurring_parent');

	/**
	 * Maps a single row on an object of the given class
	 *
	 * @param string $className The name of the target class
	 * @param array $row A single array with field_name => value pairs
	 * @return object An object of the given class
	 */
	protected function mapSingleRow($className, array $row) {
		if ($className === 'Tx_NewsRecurring_Domain_Model_News') {
			$parentRow = $this->getParentRow($row['recurring_parent']);
			/** @var \Tx_NewsRecurring_Domain_Model_News $object */
			$object = $this->createEmptyObject($className);

			$parentRow = $this->overlayRow($parentRow, $row);

			$this->thawProperties($object, $parentRow);
			$this->identityMap->registerObject($object, $row['uid']);
			return $object;
		} else {
			return parent::mapSingleRow($className, $row);
		}
	}

	/**
	 * Overlay record with fields which need to be from the recurring row itself
	 *
	 * @param array $parent parent row
	 * @param array $original current row
	 * @return array modified parent row
	 */
	protected function overlayRow(array $parent, array $original) {
		foreach ($this->overlaidFields as $fieldName) {
			$parent[$fieldName] = $original[$fieldName];
		}
		$parent['recurring_original'] = $original['uid'];

		return $parent;
	}

	/**
	 * Get the parent row
	 *
	 * @param int $uid
	 * @return array
	 */
	protected function getParentRow($uid) {
		return $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('*', 'tx_news_domain_model_news', 'uid=' . (int)$uid);
	}

}
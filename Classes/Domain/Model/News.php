<?php

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
class Tx_NewsRecurring_Domain_Model_News extends Tx_News_Domain_Model_News {

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Tx_News_Domain_Model_News>
	 * @lazy
	 */
	protected $recurring;

	/**
	 * @var Tx_News_Domain_Model_News
	 * @lazy
	 */
	protected $recurringParent;

	/** @var int */
	protected $recurringOriginal;

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getRecurring() {
		return $this->recurring;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $recurring
	 */
	public function setRecurring($recurring) {
		$this->recurring = $recurring;
	}

	/**
	 * @return Tx_News_Domain_Model_News
	 */
	public function getRecurringParent() {
		return $this->recurringParent;
	}

	/**
	 * @param Tx_News_Domain_Model_News $recurringParent
	 */
	public function setRecurringParent($recurringParent) {
		$this->recurringParent = $recurringParent;
	}

	/**
	 * @return int
	 */
	public function getRecurringOriginal() {
		return $this->recurringOriginal;
	}

	/**
	 * @param int $recurringOriginal
	 */
	public function setRecurringOriginal($recurringOriginal) {
		$this->recurringOriginal = $recurringOriginal;
	}

}
<?php

namespace GeorgRinger\NewsRecurring\Hooks;

use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Class Label
 */
class Label {

	/**
	 * @param array $params
	 * @return void
	 */
	public function getNewsLabel(array &$params) {
		if ((int)$params['row']['type'] === 7) {
			$date = (int)$params['row']['datetime'] > 0 ? $params['row']['datetime'] : $GLOBALS['EXEC_TIME'];

			$params['title'] = BackendUtility::datetime($date);
		} else {
			$params['title'] = $params['row']['title'];
		}
	}

}